package Assignment1;

import java.util.ArrayList;

public class Palindrome implements Comparable{
	String palindrome;
	int startPosition;
	int endPosition;
	ArrayList<Integer> midPoint;
	
	public Palindrome(String pString, int start, int end){
		this.palindrome = new String(pString);
		this.startPosition = start;
		this.endPosition = end;
		this.midPoint = new ArrayList<Integer>();
		this.midPoint.add(start + (Math.abs(end-start)/2));
	}

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Palindrome other = (Palindrome) arg0;
		if(this.palindrome.equals(other.palindrome)){
			this.midPoint.add(other.startPosition - ((other.endPosition-other.startPosition)/2));
			return 0;
		}
		if(this.palindrome.length() > other.palindrome.length()){
			return -1;
		}
		return 1;
	}
}

