// Name: Vaghasia, Anish
// EID: AV23674
// EE422C-Assignment 1
package Assignment1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class A1Driver 
{
	ArrayList<Integer> charMap[];
	Set<Palindrome> palindromeList;
	String cleanString;
	StringBuilder mapString;
	Set<Integer> midPoints = new HashSet<Integer>();
	
	public static void main (String args[]) 
	{ 
		if (args.length != 1) 
		{
			System.err.println ("Error: Incorrect number of command line arguments");
			System.exit(-1);
		}
		processLinesInFile (args[0]);
	}

	/**
	 * Opens the file specified in String filename, reads each line in it
	 * Invokes parse () on each line in the file, and prints out the  
	 * string with all discovered palindromes in it.
	 * @param filename - the name of the file that needs to be read
	 */
	public static void processLinesInFile (String filename) 
	{ 

		A1Driver myPalFinder = new A1Driver(); 
		try 
		{
			FileReader freader = new FileReader(filename);
			BufferedReader reader = new BufferedReader(freader);
			
			for (String s = reader.readLine(); s != null; s = reader.readLine()) 
			{
				System.out.println("The input string is: " + s);
				String palindromes = myPalFinder.parse(s);
				System.out.println("The Palindromes found: " + palindromes);
			} 
		} 
		catch (FileNotFoundException e) 
		{
			System.err.println ("Error: File not found. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		} 
                catch (IOException e) 
		{
			System.err.println ("Error: IO exception. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	/**
	 * Parses the inputString to find all palindromes based on rules specified 
	 * in your assignment write-up. 
	 * 
	 * @param inputString - the String that needs to be parsed to discover palindromes 
	 * 		
	 * @return the String object containing the Palindromes found in the
	 *         inputString    
	 */
	public String parse (String inputString) 
	{ 
		// modify the following code. Add/delete anything you want after this point.
		if(inputString.equals("QUIT")){
			//TODO: check what to do 
			return "done";
		}
		String outputString = new String(inputString); // makes a copy of inputString. 
		cleanString = removeFormatting(inputString);
		if(cleanString.equals("") || cleanString.equals(null)){
			return "none";
		}
		charMap = new ArrayList[27];
		createMap();
		findPalindromes();
		checkConflicts();
		return stringPalindromes();
	}
	
	public String removeFormatting(String input){
        StringBuilder output = new StringBuilder();
        mapString = new StringBuilder();
        char temp;
        for(int i = 0; i < input.length(); i++){
            temp = input.charAt(i);
            if(temp > 96 && temp < 123){
            	temp -= 32;
            	output.append(temp);
            	temp -= 65;
                mapString.append(temp);
            }
            if(temp > 64 && temp < 91){
            	output.append(temp);
                temp -= 65;
                mapString.append(temp);
            }
        }
        return output.toString();
	}
	
	
	public void findPalindromes(){
		palindromeList = new TreeSet<Palindrome>();
		int start, end;
		for(int i = 0; i < mapString.length()-1; i++){
			char testChar = mapString.charAt(i);
			for(int k = 0; k < charMap[testChar].size(); k++){
				for(int j = 0; j < charMap[testChar].size() - 1; j++){
					start = (int) charMap[testChar].get(j);
					end = (int) charMap[testChar].get(charMap[testChar].size() - 1);
					if(checkPosition(start, end)){
						palindromeList.add(new Palindrome(cleanString.substring(start, end+1), start, end));
					}
				}
			}
		}
	}
	
	public Boolean checkPosition(int startPos, int endPos){
		if(Math.abs(endPos - startPos) < 2){
			return false;
		}
		//if((endPos - startPos)%2 == 1){
		while((endPos - startPos) >= 0){
			if(cleanString.charAt(startPos) != cleanString.charAt(endPos)){
				return false;
			}
			startPos++;
			endPos--;
		}
		return true;
	}
	
	public void checkConflicts(){
		Object[] pList = palindromeList.toArray();
		Palindrome current, check;
		for(int j = 0; j < palindromeList.size(); j++){
			current = (Palindrome) pList[j];
			for (int i = palindromeList.size() - 1; i > j; i--){
				check = (Palindrome) pList[i];
				if(checkPalindromes(current.midPoint, check.midPoint)){
					palindromeList.remove(check);
				}
			}
		}
	}
	
	public boolean checkPalindromes(ArrayList<Integer> midPoint1, ArrayList<Integer> midPoint2){
		for(int i = 0; i < midPoint1.size(); i++){
			if(midPoint2.contains(midPoint1.get(i))){
				return true;
			}
		}
		return false;
	}
	
	public String stringPalindromes(){
		StringBuilder Palindromes = new StringBuilder();
		Set<String> sortedList = new TreeSet<String>();
		Iterator<Palindrome> itr = palindromeList.iterator();
		while(itr.hasNext()){
			Palindrome x = itr.next();
			sortedList.add(x.palindrome);
		}
		
		Iterator<String> itrStr = sortedList.iterator();
		while(itrStr.hasNext()){
			Palindromes.append(itrStr.next() + " ");
		}
		return Palindromes.toString();
	}
	
	public void createMap(){
		char temp;
        for(int i = 0; i < mapString.length(); i++){
            temp = mapString.charAt(i);
            if(charMap[temp] == null){
            	charMap[temp] = new ArrayList<Integer>();
            }
            charMap[temp].add((Integer)i);
        }
	}
	
	public void printMap(){
		for(int i = 0; i < charMap.length; i++){
			if(charMap[i] != null){
				System.out.print((char) (i + 65) + " :: ");
				for (int j = 0; j < charMap[i].size(); j++){
					System.out.print((charMap[i].get(j)) + ":");
				}
				System.out.println();
			}
		}
	}
}
